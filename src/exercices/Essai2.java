package exercices;

import donnees.Dao;
import entites.Club;
import java.util.Scanner;

public class Essai2 {


    public static void main(String[] args) {

        Scanner clavier = new Scanner(System.in);
        System.out.println("Entrer le code du club : ");
        String cc = clavier.next();
        
        
        
        Club c = Dao.getClubDeCode(cc);
        
        System.out.println(c.getNomClub());
        System.out.println(c.getAdrClub());
        
        System.out.println();
        
        System.out.println("Nombre de judokas : "+c.nbJudokas());;
        System.out.println("Nombre d'hommes judokas : " +c.nbJudokas("M"));
        System.out.println("Nombre de femmes judokas : " +c.nbJudokas("F"));
        
        
        
    }
}
