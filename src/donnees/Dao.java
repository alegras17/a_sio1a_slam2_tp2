package donnees;

import entites.Club;
import entites.Judoka;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class Dao {
      
 private static EntityManager  em=Persistence.createEntityManagerFactory("PU").createEntityManager();
   
 public static List<Judoka> getTousLesJudokas() {
     
     return em.createQuery("Select j from Judoka j").getResultList();
 }
 
 public static List<Club>   getTousLesClubs() {
     
      return em.createQuery("Select c from Club c").getResultList();
 }
 
 public static Judoka       getJudokaNumero(Long pId){
     
     return em.find(Judoka.class, pId);
 }
 
 public static Club         getClubDeCode (String pCode){
     
     return em.find(Club.class,pCode);
 } 
}



