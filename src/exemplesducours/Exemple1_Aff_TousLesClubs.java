package exemplesducours;

import donnees.Dao;
import entites.Club;

public class Exemple1_Aff_TousLesClubs {
    
    public static void main(String[] args) {
        
       System.out.println("Liste de tous les clubs\n");
        
       for (Club club : Dao.getTousLesClubs()){
       
           club.affichageConsole();
           System.out.println();
       } 
        
       System.out.println();
    }
}

