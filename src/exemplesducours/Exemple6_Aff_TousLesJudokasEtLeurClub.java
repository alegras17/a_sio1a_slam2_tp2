package exemplesducours;

import donnees.Dao;
import entites.Judoka;
import static utilitaires.UtilDate.aujourdhuiChaine;

public class Exemple6_Aff_TousLesJudokasEtLeurClub {

    public static void main(String[] args) {
         
        String format= " Liste de tous les Judokas ( tous clubs confondus ) à la date du: %-10s \n\n";
       
        System.out.printf(format, aujourdhuiChaine());
     
        for( Judoka judoka : Dao.getTousLesJudokas() ) {
            
            judoka.affichageConsole();
            
            System.out.print(" "+judoka.getLeClub().getNomClub());
            
            System.out.println();
        }     
        System.out.println();
    }
}


