package entites;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import static utilitaires.UtilDojo.determineCategorie;


@Entity
public class Club implements Serializable {
   
  @Id
  private String         codeClub;
    
  private String         nomClub;
  private String         adrClub;
    
  @OneToMany(mappedBy = "leClub")
  private List<Judoka>   lesJudokas= new LinkedList();
          
  public Club() {}
      
  //<editor-fold defaultstate="collapsed" desc="Gets Sets">
  
  public String getCodeClub() {
      return codeClub;
  }
  
  public void setCodeClub(String codeClub) {
      this.codeClub = codeClub;
  }
  
  public String getNomClub() {
      return nomClub;
  }
  
  public void setNomClub(String nomClub) {
      this.nomClub = nomClub;
  }
  
  public String getAdrClub() {
      return adrClub;
  }
  
  public void setAdrClub(String adrClub) {
      this.adrClub = adrClub;
  }
  
  public List<Judoka> getLesJudokas() {
        return lesJudokas;
    }

  public void setLesJudokas(List<Judoka> lesJudokas) {
        this.lesJudokas = lesJudokas;
  }
  
  
  //</editor-fold>
  
  
  //<editor-fold defaultstate="collapsed" desc="Méthodes publiques">
  
  public void affichageConsole(){
      
      System.out.printf("%-8s %-20s %-40s", codeClub,nomClub,adrClub);
  }
 	
  public int nbJudokas(){
      
      int nbpers=0;
      
      for (Judoka j : lesJudokas){
                           nbpers ++;
          }
      return nbpers; 
      }
  
public int nbJudokas(String pSexe){
    int nbJdk=0;
    for (Judoka j : lesJudokas){
          
          if( j.getSexe().equals(pSexe)){
          nbJdk++;                 
          }
    }
    return nbJdk;
}

public List<Judoka> lesJudokasDeCategorie(String pCategorie){
    
    List<Judoka> lesJudokasDeCategorie = new LinkedList();
    for(Judoka j : lesJudokas){
        
        if(determineCategorie(j.getSexe(),j.getPoids()).equals(pCategorie)) lesJudokasDeCategorie.add(j);
    }
      return lesJudokasDeCategorie;
        
}
  
  
  
  //</editor-fold>
  //<editor-fold defaultstate="collapsed" desc="HashCode & Equals">
  
  @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + (this.codeClub != null ? this.codeClub.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Club other = (Club) obj;
        if ((this.codeClub == null) ? (other.codeClub != null) : !this.codeClub.equals(other.codeClub)) {
            return false;
        }
        return true;
    }
  
  //</editor-fold>     
}


