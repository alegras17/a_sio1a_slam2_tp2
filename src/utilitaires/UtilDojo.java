package utilitaires;

public class UtilDojo {
    
   public static   String   determineCategorie(String sexe, int poids){
     
       return libellesCategories[determineIndiceCategorie(sexe,poids)];       
   }  
   
   public static   int      determineIndiceCategorie(String sexe, int poids){
     

     int i;
         
     if (  sexe.equals("M") )
     {         
        i = chercheIndice( poids, limitesHommes );   
     }   
     else
     {   
        i = chercheIndice( poids, limitesFemmes );
     }   
         
     return i; 
    
   }  
   
   public static   String[] getLibellesCategories() {
       return libellesCategories;
   }  
   
   //<editor-fold defaultstate="collapsed" desc="Table des libelles des categories">
   
    private static  String[] libellesCategories = { "super-légers","mi-légers","légers",
                                                    "mi-moyens"   ,"moyens",
                                                    "mi-lourd"    ,"lourds"
                                                  }; 
   //</editor-fold>
   
   //<editor-fold defaultstate="collapsed" desc="Tables des limites de poids">
  
    static  int[]    limitesHommes = {60, 66, 73, 81, 90, 100};
    static  int[]    limitesFemmes = {48, 52, 57, 63, 70, 78};
   
   //</editor-fold>
    
   //<editor-fold defaultstate="collapsed" desc="Reste du code">  
   
   private static int    chercheIndice(int poids, int[] tableauLimites ){
   
      int i;
      
      for(i=0;i<tableauLimites.length;i++)
      {         
         if( poids<tableauLimites[i] )
         {  
            break;
         }
      }
   
      return i;
   }
   
    //</editor-fold>  
}

